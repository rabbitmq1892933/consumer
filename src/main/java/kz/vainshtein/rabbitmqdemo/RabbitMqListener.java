package kz.vainshtein.rabbitmqdemo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RabbitMqListener {

    @RabbitListener(queues = "queue1")
    public void processMessage(String message) {
        log.info("Message received: {}", message);
    }
}
